import random
pet_dict=dict()
class Pet:
    boredom_thre=30
    hunger_thre=35
    boredom_decrement=10
    hunger_decrement=10
    def __init__(self,sound):
        self.boredom=random.randint(0,Pet.boredom_thre)
        self.hunger=random.randint(0,Pet.hunger_thre)
        self.sound=[]
        self.sound.append(sound)
        self.words=[]
        
    def clock_tic(self):
        self.boredom+=3
        self.hunger+=3

    def hi(self):
        if len(self.words)>0:
            s=random.choice(self.words)
            print(s)
            print('\n')
        else:
            print('No words have been taught')
            print('\n')
        self.reduce_boredom()
    
    def teach(self,word):
        self.words.append(word)
        print('I have learned a new word')
        print('\n')
        self.reduce_boredom()
    
    def reduce_boredom(self):
        var=self.boredom-Pet.boredom_decrement
        if var>=0:
            self.boredom=var
        else:
            print('Can not go below zero')
            print('\n')
            
    
    def reduce_hunger(self):
        var=self.hunger-Pet.hunger_decrement
        if var>=0:
            self.hunger=var
        else:
            print('Can not go below zero')
            print('\n')
    
    def feed(self):
        self.reduce_hunger()
    
    def __str__(self):
        if self.boredom>Pet.boredom_thre:
            if self.hunger>Pet.hunger_thre:
                return 'I am  HUNGRY and BORED'
            else:
                return 'I am BORED'
        elif self.hunger>Pet.hunger_thre:
            return 'I am HUNGRY'
        else:
            return 'Iam neither Hungry nor Bored'

def create():
    name=str(input('enter the name of the pet'))
    snd=str(input('enter the sound of the pet'))
    n=name
    n=Pet(snd)
    pet_dict[name]=n
    n.clock_tic()
    
def main():
    print('1.Create pet')
    print('2.Check hungry/bored status of a Pet')
    print('3.Feed pet')
    print('4.Teach pet')
    print('5.Hi pet')
    n=int(input('Enter Option'))
    if n==1:
        create()
        
    if n==2:
        print(pet_dict.keys())
        if len(pet_dict)==0:
            print('no pets have been added')
        elif len(pet_dict)>0:
            n=str(input('enter the name of the pet from the above list'))
            print(pet_dict[n])
            print('\n')
            for i in pet_dict:
                pet_dict[i].clock_tic()
                
        
    if n==3:
        print(pet_dict.keys())
        n=str(input('enter the name of the pet from the above list'))
        print(f'Feeding {n}')
        pet_dict[n].feed()
        print(pet_dict[n])
        print('\n')
        for i in pet_dict:
            pet_dict[i].clock_tic()

    if n==4:
        print(pet_dict.keys())
        n=str(input('enter the name of the pet from the above list'))
        word=str(input(f'Enter the word you want {n} to learn'))
        pet_dict[n].teach(word)
        print(pet_dict[n])
        print('\n')
        for i in pet_dict:
            pet_dict[i].clock_tic()

    if n==5:
        print(pet_dict.keys())
        n=str(input('enter the name of the pet from the above list'))
        pet_dict[n].hi()
        for i in pet_dict:
            pet_dict[i].clock_tic()
        
        
    

while True:
    main()





    
